from mongomock import MongoClient

class SessionManager:
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client['testdb']
        self.sessions = self.db['sessions']

    def create_session(self, user_id):
        session_data = {"user_id": user_id, "data": {}}
        result = self.sessions.insert_one(session_data)
        return str(result.inserted_id)

    def get_session(self, session_id):
        session = self.sessions.find_one({"_id": session_id})
        return session

    def update_session_data(self, session_id, key, value):
        self.sessions.update_one({"_id": session_id}, {"$set": {"data." + key: value}})

    def close_session(self, session_id):
        self.sessions.delete_one({"_id": session_id})

# Example usage
session_manager = SessionManager()

# Create a session
session_id = session_manager.create_session("user123")
print("Created session with ID:", session_id)

# Get session data
session = session_manager.get_session(session_id)
print("Session data:", session)

# Update session data
session_manager.update_session_data(session_id, "cart_items", ["item1", "item2"])
session = session_manager.get_session(session_id)
print("Updated session data:", session)

# Close session
session_manager.close_session(session_id)
print("Session closed")

